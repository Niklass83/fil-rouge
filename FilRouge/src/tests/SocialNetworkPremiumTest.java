package tests;


public class SocialNetworkPremiumTest {
    /**
     * @param args not used
     */
    public static void main(String[] args) {

        try {
            TestReport testSuiteReport = new TestReport(0, 0);
            TestReport tr;
            tr = InitTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = AddMemberTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = AddItemFilmTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = ReviewItemFilmTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = AddItemBookTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = ReviewItemBookTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = ReviewOpinionFilmTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = ReviewOpinionBookTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");
            //
            tr = TimeTest.test();
            testSuiteReport.add(tr);
            System.out.println("\n\n **********************************************************************************************\n");


            // End of the test suite : give some feedback to the tester
            System.out.println("Global tests results :   \n" + testSuiteReport);
        } catch (Exception e) {
            System.out.println("ERROR : Some exception was throw unexpectedly");
        }

    }

}