package tests;

import exceptions.*;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;

public class TimeTest {



    public static long ajoutMembresTest(ISocialNetwork sn, int nombreMembre) throws BadEntryException, MemberAlreadyExistsException {
        long time1= System.currentTimeMillis();
        long time2=0;
        for(int i=0;i<nombreMembre;i++){
            sn.addMember("usr"+i,"Password","Profile");
        }
        time2=System.currentTimeMillis();
        return (time2-time1);
    }

    public static long ajoutItemTest(ISocialNetwork sn, int nombreItem) throws BadEntryException, NotMemberException , ItemFilmAlreadyExistsException {
        long time1= System.currentTimeMillis();
        long time2=0;
        for(int i=0;i<nombreItem;i++){
            sn.addItemFilm("usr1","Password","title"+i,"Type"+i,"Moi"+i,"Paul",i+1);
        }
        time2=System.currentTimeMillis();
        return (time2-time1);
    }

    public static long ajoutReviewTest(ISocialNetwork sn, int nombreMembre) throws BadEntryException, NotMemberException , NotItemException {
        long time1= System.currentTimeMillis();
        long time2=0;
        for(int i=0;i<nombreMembre;i++){
            for(int j=0;j<17;j++) {
                sn.reviewItemFilm("usr"+i,"Password","title"+i,4.0f,"Moi"+j);
            }
        }
        time2=System.currentTimeMillis();
        return (time2-time1);
    }

    public static TestReport test(){
        int nbTests=0;
        int nbErrors=0;

        try {
            int nombreMembre=500;
            System.out.println("Test Temporel avec "+nombreMembre+" Membres :\n");
            ISocialNetwork sn = new SocialNetwork();

            long testTemps = ajoutMembresTest(sn,nombreMembre);
            nbTests++;
            System.out.println("Temps execution pour ajout "+nombreMembre+" Membres : " + testTemps +" ms");

            nbTests++;
            testTemps = ajoutItemTest(sn,nombreMembre*10);
            System.out.println("Temps execution pour ajout "+nombreMembre*10+" Items : " + testTemps +" ms");

            nbTests++;
            testTemps = ajoutReviewTest(sn,nombreMembre);
            System.out.println("Temps execution pour ajout 8500 Reviews : " + testTemps +" ms");

            System.out.println("\n\n --------------------------------------------------\n");



            nombreMembre=250;
            System.out.println("Test Temporel avec "+nombreMembre+" Membres :\n");
            ISocialNetwork sn1 = new SocialNetwork();

            nbTests++;
            testTemps = ajoutMembresTest(sn1,nombreMembre);
            System.out.println("Temps execution pour ajout "+nombreMembre+" Membres : " + testTemps +" ms");

            nbTests++;
            testTemps = ajoutItemTest(sn1,nombreMembre*10);
            System.out.println("Temps execution pour ajout "+nombreMembre*10+" Items : " + testTemps +" ms");

            nbTests++;
            testTemps = ajoutReviewTest(sn1,nombreMembre);
            System.out.println("Temps execution pour ajout 4250 Reviews : " + testTemps +" ms");

            System.out.println("\n\n --------------------------------------------------\n");

            nombreMembre=1000;
            System.out.println("Test Temporel avec "+nombreMembre+" Membres :\n");
            ISocialNetwork sn2 = new SocialNetwork();

            nbTests++;
            testTemps = ajoutMembresTest(sn2,nombreMembre);
            System.out.println("Temps execution pour ajout "+nombreMembre+" Membres : " + testTemps +" ms");

            nbTests++;
            testTemps = ajoutItemTest(sn2,nombreMembre*10);
            System.out.println("Temps execution pour ajout "+nombreMembre*10+" Items : " + testTemps +" ms");

            nbTests++;
            testTemps = ajoutReviewTest(sn2,nombreMembre);
            System.out.println("Temps execution pour ajout "+nombreMembre*17+" Reviews : " + testTemps +" ms");
        }
        catch(Exception e){System.out.println("ERROR : Some exception was throw unexpectedly");}

        // Print a summary of the tests and return test results
        try{
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("\nTime Test : " + tr);
            return tr;
        }
        catch (NotTestReportException e){ //This shouldn't happen
            System.out.println("Unexpected error in AddMemberTest test code - Can't return valuable test results");
            return null;
        }
    }

    public static void main (String[] args){
        test();
    }
}
