package tests;

import exceptions.*;
import opinion.ISocialNetworkPremium;
import opinion.SocialNetworkPremium;

public class ReviewOpinionFilmTest {

    private static int BadEntryExceptionTest(ISocialNetworkPremium sn, String login,
                                             String pwd, String loginCommentAdd,String commentAddOpinion, String title, float mark, String comment, String testId, String errorMessage) {
        try {
            sn.reviewOpinionFilm(login, pwd, loginCommentAdd, commentAddOpinion,title ,mark,comment); // Try to add review on this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (BadEntryException e) { // BadEntry exception was thrown by

            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }

    private static int reviewOpinionFilmOKTest(ISocialNetworkPremium sn, String login,
                                               String pwd, String loginCommentAdd, String commentAddOpinion, String title,float mark,String comment, String testId, String errorMessage) {

        try {
            sn.reviewOpinionFilm(login, pwd, loginCommentAdd, commentAddOpinion,title , mark, comment); // Try to add this member
            // No exception was thrown. That's a good start !
            return 0; // return success code : everything is OK, nothing to
            // display
        } catch (Exception e) {// An exception was thrown by addMember() : this
            // is an error case
            System.out
                    .println("Err " + testId + " : unexpected exception " + e); // Error
            // message
            // displayed
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error code
        }
    }

    private static int NotMemberExceptionTest(ISocialNetworkPremium sn, String login,
                                              String pwd, String loginCommentAdd, String commentAddOpinion, String title,float mark,String comment, String testId, String errorMessage) {
        try {
            sn.reviewOpinionFilm(login, pwd, loginCommentAdd, commentAddOpinion,title ,mark,comment); // Try to add this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotMemberException e) { // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }

    }

    private static int NotItemExceptionTest(ISocialNetworkPremium sn, String login,
                                            String pwd, String loginCommentAdd, String commentAddOpinion, String title,float mark,String comment, String testId, String errorMessage) {
        try {
            sn.reviewOpinionFilm(login, pwd, loginCommentAdd, commentAddOpinion,title ,mark,comment); // Try to add this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotItemException e) { // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }



    public static TestReport test(){

        ISocialNetworkPremium sn = new SocialNetworkPremium();


        int nbTests = 0; // total number of performed tests
        int nbErrors = 0; // total number of failed tests


        try {
            // Add member for testing
            sn.addMember("Paul", "12345", "SuperUser");
            sn.addMember("Nicholas", "45678", "SuperUser");
            sn.addMember("Thomas", "password", "SuperUser");


            // Add Book for testing
            sn.addItemBook("Paul", "12345", "American History X", "Thriller", "Patrick Timsit", 350);

            // Add Movie for testing
            sn.addItemFilm("Paul", "12345", "Nocturnal Animals", "Thriller", "Tom Ford", "Amy Adams",120);
            sn.addItemFilm("Paul", "12345", "Asterix et Obélix", "Comedie", "Alain Chabat", "Paul Newman",120);

            // Add Comment on Movie for testing
            sn.reviewItemFilm("Paul","12345","Nocturnal Animals",2.5f,"Bon film");
            sn.reviewItemFilm("Nicholas","45678","Nocturnal Animals",5f,"Une tuerie");


            sn.reviewItemFilm("Nicholas","45678","Asterix et Obélix",4f,"Vraiment bien");
            sn.reviewItemFilm("Paul","12345","Asterix et Obélix",2.5f,"Bon film");

        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
        }

        System.out.println("Testing reviewOpinionFilm()");


        // <=> test n°1

        // check if incorrect parameters cause reviewItemBook() cause BadEntryException
        // exception



        // Add review Opinion on Movie Nocturnal Animals at Nicholas
        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Paul", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 2.1, "C'est vraiment un bon choix","8.1",
                "reviewOpinionFilm didn't add correctly comment");

        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Paul", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.3",
                "reviewOpinionFilm didn't add correctly comment");

        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Thomas", "password", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 4.5, "Cool film","8.2",
                "reviewOpinionFilm didn't add correctly comment");


        // Add review Opinion on Movie Nocturnal Animals at Paul
        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Nicholas", "45678", "Paul","Bon film", "Nocturnal Animals", (float) 4.1, "C'est de la bombe","8.4",
                "reviewOpinionFilm didn't add correctly comment");

        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Thomas", "password", "Paul","Bon film", "Nocturnal Animals", (float) 2.1, "Pas mal","8.5",
                "reviewOpinionFilm didn't add correctly comment");

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Add review Opinion on Movie Asterix et Obelix at Nicholas
        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Paul", "12345", "Nicholas","Vraiment bien", "Asterix et Obélix", (float) 2.5, "J'aime Bien","8.6",
                "reviewOpinionFilm didn't add correctly comment");

        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Thomas", "password", "Nicholas","Vraiment bien", "Asterix et Obélix", (float) 4.5, "Cool film","8.7",
                "reviewOpinionFilm didn't add correctly comment");


        // Add review Opinion on Movie Asterix et Obelix at Paul
        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Nicholas", "45678", "Paul","Bon film", "Asterix et Obélix", (float) 1.1, "Pour la classe","8.8",
                "reviewOpinionFilm didn't add correctly comment");

        nbTests++;
        nbErrors += reviewOpinionFilmOKTest(sn, "Thomas", "password", "Paul","Bon film", "Asterix et Obélix", (float) 2.1, "Pas mal","8.9",
                "reviewOpinionFilm didn't add correctly comment");







        // Password minus 4 caractere

        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", " ","Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.10",
                "reviewOpinionFilm doesn't take care of password");

        // Bad login
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Pa uL", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.11",
                "reviewOpinionFilm doesn't take care of trailing blanks and leadings.");

        // Bad login only trailing blanks
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "   ", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.11 bis",
                "reviewOpinionFilm doesn't take care of trailing blanks and leadings.");

        // Bad login only trailing blanks
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "   ","Une tuerie", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.11 ter",
                "reviewOpinionFilm doesn't take care of trailing blanks and leadings.");

        // Mark outside the limit
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 7.1, "C'est vraiment bien","8.12",
                "reviewOpinionFilm doesn't take care of mark outside the limit");

        // Bad login for Member who add review opinion
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paul", "12345", "Nic hoLas","Une tuerie", "Nocturnal Animals", (float) 4.1, "C'est vraiment bien","8.13",
                "reviewOpinionFilm doesn't take care of mark outside the limit");

        // Title Null
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas","Une tuerie", null, (float) 3.1, "C'est vraiment bien","8.14",
                "reviewOpinionFilm doesn't take care of title Null");

        // Without Comments
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, null,"8.15",
                "reviewOpinionFilm doesn't take if there is comment or not");

        // Bad Password
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paul", "BadPassword", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.16",
                "reviewOpinionFilm doesn't take care of bad password");

        // Good Password but bad login
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paulo", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.17",
                "reviewOpinionFilm doesn't take care of bad login and good password");

        // Book instead of a Movie
        nbTests++;
        nbErrors += NotItemExceptionTest(sn, "Paul", "12345", "Nicholas","Une tuerie", "American History X", (float) 3.1, "C'est vraiment bien","8.18",
                "reviewOpinionFilm doesn't take care of book instead of movie");

        // Test commentaire de base Null
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas",null, "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","8.19",
                "reviewOpinionFilm doesn't take care if comment is null");

        // Tes commentaire Opinion null
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas","Une tuerie", "Nocturnal Animals", (float) 3.1, null,"8.20",
                "reviewOpinionFilm doesn't take care if comment Opinion is null");


        // Display final state of 'sn'
        System.out.println("Final state of the social network : " + sn);

        // Print a summary of the tests and return test results
        try{
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("Review Opinion Film Test : " + tr);
            return tr;
        }
        catch (NotTestReportException e){ //This shouldn't happen
            System.out.println("Unexpected error in ReviewItemBook test code - Can't return valuable test results");
            return null;
        }

    }


    /**
     * Launches test()
     * @param args not used
     */
    public static void main(String[] args) {
        test();
    }

}
