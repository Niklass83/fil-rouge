package tests;

import exceptions.BadEntryException;
import exceptions.NotItemException;
import exceptions.NotMemberException;
import exceptions.NotTestReportException;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;

import java.util.LinkedList;

public class ReviewItemBookTest {

    //reviewItemBook(String login, String password, String title, float mark, String comment): float
    //reviewItemBook(login, password, title, mark, comment)

    /**
     * @param sn
     * @param login
     * @param pwd
     * @param title
     * @param mark
     * @param comment
     * @param testId
     * @param errorMessage
     * @return value of the error
     *
     * With BadEntryExceptionTest we will test every kind of error and will return 0 if BadEntryException is thrown or 1 if the this exception
     * isn't thrown
     */
    private static int BadEntryExceptionTest(ISocialNetwork sn, String login,
                                             String pwd, String title, float mark, String comment, String testId, String errorMessage) {
        try {
            sn.reviewItemBook(login, pwd, title, mark,comment); // Try to add review on this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (BadEntryException e) { // BadEntry exception was thrown by

            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }

    /**
     * @param sn
     * @param login
     * @param pwd
     * @param title
     * @param mark
     * @param comment
     * @param testId
     * @param errorMessage
     * @return the value of the error
     *
     * reviewItemBookOKTest check if with good parameters we can create a new review on an already created Book
     *
     */
    private static int reviewItemBookOKTest(ISocialNetwork sn, String login,
                                            String pwd, String title, float mark, String comment, String testId, String errorMessage) {

        try {
            float test = sn.reviewItemBook(login, pwd, title, mark, comment); // Try to add this member
            // No exception was thrown. That's a good start !
            if (test == -1) { // But the number of members
                // hasn't changed
                // accordingly
                System.out.println("Err " + testId); // Error message displayed
                return 1; // return error code
            } else
                return 0; // return success code : everything is OK, nothing to
            // display
        } catch (Exception e) {// An exception was thrown by addMember() : this
            // is an error case
            System.out
                    .println("Err " + testId + " : unexpected exception " + e); // Error
            // message
            // displayed
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error code
        }
    }


    /**
     * @param sn
     * @param login
     * @param pwd
     * @param title
     * @param mark
     * @param comment
     * @param testId
     * @param errorMessage
     * @return the value of the error
     *
     * With NotMemberExceptionTest we will test every kind of error and will return 0 if NotMemberException is thrown or 1 if the this exception
     * isn't thrown
     *
     */
    private static int NotMemberExceptionTest(ISocialNetwork sn, String login,
                                              String pwd, String title, float mark, String comment, String testId, String errorMessage) {
        try {
            sn.reviewItemBook(login, pwd, title, mark,comment); // Try to add this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotMemberException e) { // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }

    }

    /**
     * @param sn
     * @param login
     * @param pwd
     * @param title
     * @param mark
     * @param comment
     * @param testId
     * @param errorMessage
     * @return the value of the error
     *
     * With NotItemExceptionTest we will test every kind of error and will return 0 if NotItemException is thrown or 1 if the this exception
     * isn't thrown
     *
     */
    private static int NotItemExceptionTest(ISocialNetwork sn, String login,
                                            String pwd, String title, float mark, String comment, String testId, String errorMessage) {
        try {
            sn.reviewItemBook(login, pwd, title, mark,comment); // Try to add this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotItemException e) { // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }

    /**
     * @return TestReport
     *
     * In this method we will test all the different kind of Exception we want to throw
     * At the end we will have the number of test we made and the number of error we made
     * Like that we could check if our program is good or not.
     *
     */
    public static TestReport test(){

        ISocialNetwork sn = new SocialNetwork();


        int nbTests = 0; // total number of performed tests
        int nbErrors = 0; // total number of failed tests


        try {
            // Add member for testing
            sn.addMember("Paul", "12345", "SuperUser");
            sn.addMember("Nicholas", "45678", "SuperUser");


            // Add Book for testing
            sn.addItemBook("Paul", "12345", "American History X", "Thriller", "Patrick Timsit", 350);

            // Add Movie for testing
            sn.addItemFilm("Paul", "12345", "Nocturnal Animals", "Thriller", "Tom Ford", "Amy Adams",120);

        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
        }

        System.out.println("Testing reviewItemBook()");


        // <=> test n°1

        // check if incorrect parameters cause reviewItemBook() cause BadEntryException
        // exception


        nbTests++;
        nbErrors += reviewItemBookOKTest(sn, "Paul", "12345", "American History X", (float) 1.1,"C'est vraiment un bon Book (commentaire)","5.1",
                "reviewItemBook add correctly comment");

        nbTests++;
        nbErrors += reviewItemBookOKTest(sn, "Paul", "12345", "American History X", (float) 3.1,"Super Book (commentaire)","5.3",
                "reviewItemBook change correctly comment");

        nbTests++;
        nbErrors += reviewItemBookOKTest(sn, "Nicholas", "45678", "American History X", (float) 1.1,"Best Book Ever","5.2",
                "reviewItemBook add correctly comment with last member");




        // Password minus 4 caractere

        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", " ", "American History X", (float) 1.1,"C'est vraiment un bon Book (commentaire)","5.4",
                "reviewItemBook doesn't take care of password");

        // Bad login
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Pa uL", "12345", "American History X", (float) 1.1,"C'est vraiment un bon Book (commentaire)","5.5",
                "reviewItemBook doesn't take care of trailing blanks and leadings.");

        // Bad login
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "   ", "12345", "American History X", (float) 1.1,"C'est vraiment un bon Book (commentaire)","5.5 bis",
                "reviewItemBook doesn't take care of trailing blanks and leadings.");

        // Mark outside the limit
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "American History X", (float) 6.5,"C'est vraiment un bon Book (commentaire)","5.6",
                "reviewItemBook doesn't take care of mark outside the limit");

        // Title Null
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", null, (float) 4.5,"C'est vraiment un bon Book (commentaire)","5.7",
                "reviewItemBook doesn't take care of title Null");

        // Title Trailing Blanks
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "   ", (float) 4.5,"C'est vraiment un bon Book (commentaire)","5.7 bis",
                "reviewItemBook doesn't take care of title with only space");

        // Without Comments
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "American History X", (float) 1.1,null,"5.8",
                "reviewItemBook doesn't take if there is comment or not");

        // Bad Password
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paul", "BadPassword", "American History X", (float) 1.1,"C'est vraiment un bon Book (commentaire)","5.9",
                "reviewItemBook doesn't take care of bad password");

        // Good Password but bad login
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paulo", "12345", "American History X", (float) 1.1,"C'est vraiment un bon Book (commentaire)","5.10",
                "reviewItemBook doesn't take care of bad login and good password");

        // Movie instead of a Book
        nbTests++;
        nbErrors += NotItemExceptionTest(sn, "Paul", "12345", "Nocturnal Animals", (float) 1.1,"C'est vraiment un bon Book (commentaire)","5.11",
                "reviewItemBook doesn't take care of book instead of Book");


        // Test Consult Item Book
        // Doit afficher [American History X, [Commentaire : Super Book (commentaire) Notes : 1.1, Commentaire : Best Book Ever Notes : 1.1]]
        nbTests++;
        LinkedList<String> testConsult = new LinkedList<String>();
        try {
            testConsult = sn.consultItems("American History X");
            System.out.println(testConsult.toString());
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
        }

        // Display final state of 'sn'
        System.out.println("Final state of the social network : " + sn);

        // Print a summary of the tests and return test results
        try{
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("ReviewItemBook : " + tr);
            return tr;
        }
        catch (NotTestReportException e){ //This shouldn't happen
            System.out.println("Unexpected error in ReviewItemBook test code - Can't return valuable test results");
            return null;
        }

    }


    /**
     * Launches test()
     * @param args not used
     */
    public static void main(String[] args) {
        test();
    }
}
