
package tests;

import exceptions.BadEntryException;
import exceptions.ItemBookAlreadyExistsException;
import exceptions.NotMemberException;
import exceptions.NotTestReportException;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;


public class AddItemBookTest {
    /**
     * @param sn           socialNetwork to test
     * @param login        a member from the socialNtework
     * @param password     a member from the socialNetwork
     * @param title        of the book to test
     * @param kind         of the book to test
     * @param author       of the book to test
     * @param nbPages      of the book to test
     * @param testId       of the test
     * @param errorMessage to display
     *
     * @return an int to increment the nbTotalTests
     *
     * Test the function addItemBook with bad parameters for the book to add and watch if the error is the expected one.
     */

    private static int addItemBookBadEntryTest(ISocialNetwork sn, String login, String password, String title, String kind, String author, int nbPages, String testId, String errorMessage) {

        int nbBooks = sn.nbBooks(); // Number of Items Book when starting to
        // run this method
        try {
            sn.addItemBook(login, password, title, kind, author, nbPages);
            System.out.println("Err " + testId + " : " + errorMessage);
            // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (BadEntryException e) {
            // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            if (sn.nbBooks() != nbBooks) {
                // The number of members has
                // changed : this is an error
                // case.
                System.out
                        .println("Err "
                                + testId
                                + " : BadEntry was thrown but the number of Books was changed"); // Display
                // a
                // specific
                // error
                // message
                return 1; // return "error" value
            } else
                // The number of members hasn't changed, which is considered a
                // good indicator that 'sn' was not modified
                return 0; // return success value : everything seems OK, nothing
            // to display
        } catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception BadEntry
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }


    /**
     * @param sn           socialNetwork to test
     * @param login        a member from the socialNtework
     * @param password     a member from the socialNetwork
     * @param title        of the book to test
     * @param kind         of the book to test
     * @param author       of the book to test
     * @param nbPages      of the book to test
     * @param testId       of the test
     * @param errorMessage to display
     *
     * @return an int to increment the nbTotalTests
     *
     * Test the function addItemBook with bad parameters for the book to add (already did) and watch if the error is the expected one.
     */
    private static int addItemBookAlreadyExistsTest(ISocialNetwork sn, String login, String password, String title, String kind, String author, int nbPages, String testId, String errorMessage) {
        int nbBooks = sn.nbBooks(); // Number of members when starting to
        // process this method
        try {

            sn.addItemBook(login, password, title, kind, author, nbPages); // Try to add this Book
            // Reaching this point means that no exception was thrown by
            // addItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (ItemBookAlreadyExistsException e) {// AlreadyExists exception was
            // thrown by addItemBooks () :
            // this is a good start!
            // Let's now check if 'sn'
            // was not impacted
            if (sn.nbBooks() != nbBooks) {
                System.out.println("Err "+ testId+ " : BookAlreadyExists was thrown, but the number of members was changed"); // Display
                // a
                // specific
                // error
                // message
                return 1;// and return the "error" value
            } else
                return 0; // return success value : everything is OK, nothing to
            // display
        } catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }


    /**
     * @param sn           socialNetwork to test
     * @param login        a member from the socialNtework
     * @param password     a member from the socialNetwork
     * @param title        of the book to test
     * @param kind         of the book to test
     * @param author       of the book to test
     * @param nbPages      of the book to test
     * @param testId       of the test
     * @param errorMessage to display
     * @return an int to increment the nbTotalTests
     *
     * Test the function addItemBook with bad parameters for the member and watch if the error is the expected one.
     */
    private static int addItemBookNotMemberException(ISocialNetwork sn, String login, String password, String title, String kind, String author, int nbPages, String testId, String errorMessage) {

        int nbBooks = sn.nbBooks();
        try {
            sn.addItemBook(login, password, title, kind, author, nbPages);
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotMemberException e) {// AlreadyExists exception was
            // thrown by addMember() :
            // this is a good start!
            // Let's nprofileow check if 'sn'
            // was not impacted
            if (sn.nbBooks() != nbBooks) {
                System.out
                        .println("Err "
                                + testId
                                + " : MemberAlreadyExists was thrown, but the number of members was changed"); // Display
                // a
                // specific
                // error
                // message
                return 1;// and return the "error" value
            } else
                return 0; // return success value : everything is OK, nothing to
            // display
        } catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }


    /**
     * @return the results of tests by displaying them
     */
    public static TestReport test(){

        ISocialNetwork sn = new SocialNetwork();

        // number of books in 'sn' (should be 0
        // here)
        int nbBooks = sn.nbBooks(); // number of Books in 'sn' (should be 0
        // here)

        int nbTests = 0; // total number of performed tests
        int nbErrors = 0; // total number of failed tests
        try {
            // Add member for testing
            sn.addMember("Antoine", "qsdfgh","grand amoureux" );
            //sn.addItemBook("Antoine", "qsdfgh", "title", "adventure", "director1", "scenarist", 1);
            sn.addItemBook("Antoine", "qsdfgh", "titreLivre", "aventure", "author1", 50);
            nbBooks++;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
        }

        System.out.println("Testing addItemBook()");

        //3
        nbTests++;
        nbErrors += addItemBookBadEntryTest(sn, null, "qsdfgh", "titreLivre", "aventure", "author1", 50, "6.3", "addItemBook() doesn't reject null pseudo");
        //4
        nbTests++;
        nbErrors += addItemBookBadEntryTest(sn, " ", "qsdfgh", "titreLivre", "aventure", "author1", 50,  "6.4", "addItemBook() doesn't reject logins that don't contain at least one character other than space");
        //5
        nbTests++;
        nbErrors += addItemBookBadEntryTest(sn, "Antoine", null, "titreLivre", "aventure", "author1", 50, "6.5", "addItemBook() doesn't reject null password");
        //6
        nbTests++;
        nbErrors += addItemBookBadEntryTest(sn, "Antoine", "qsd", "titreLivre", "aventure", "author1", 50,"6.6", "addItemBook() doesn't reject passwords that don't contain at least 4 characters (not taking into account leading or trailing blanks)");
        //7
        nbTests++;
        nbErrors += addItemBookBadEntryTest(sn, "Antoine", "qsdfgh", "titreLivre", null , "author1", 50, "6.7", "addItemBook() doesn't reject null kind");
        //8
        nbTests++;
        nbErrors += addItemBookBadEntryTest(sn, "Antoine", "qsdfgh", "titreLivre", "aventure", null, 50,"6.8", "addItemBook() doesn't reject null author");

        //9
        nbTests++;
        nbErrors += addItemBookBadEntryTest(sn, "Antoine", "qsdfgh", "titreLivre", "aventure", "author1", -50, "6.9", "addItemBook() doesn't reject negative nbPage");
        //10
        nbTests++;
        nbErrors += addItemBookAlreadyExistsTest(sn, "Antoine", "qsdfgh","titreLivre", "aventure", "author1", 50,"6.10", "addItemBook() add a Book already in list (first place)");
        //11
        nbTests++;
        nbErrors += addItemBookAlreadyExistsTest(sn, "Antoine", "qsdfgh","titreLivre", "aventure", "author1", 50, "6.11", "addItemBook() add a Book already in list (last place)");
        //12
        nbTests++;
        nbErrors += addItemBookAlreadyExistsTest(sn, "Antoine", "qsdfgh","tiTReLiVRe", "aventure", "author1", 50, "6.12", "addItemBook() add a Book already in list (casse sensitive)");
        //14
        nbTests++;
        nbErrors += addItemBookNotMemberException(sn, "AntOI ne", "qsdfgh","titreLivre", "aventure", "author1", 50, "6.14", "addItemBook() add a Book with the same user with different case");
        //15
        nbTests++;
        nbErrors += addItemBookNotMemberException(sn, "Martine", "qsdfgh","titreLivre", "aventure", "author1", 50, "6.15", "addItemBook() add by a non-user");
        //16
        nbTests++;
        nbErrors += addItemBookNotMemberException(sn, "Antoine", "dsdgdsh","titreLivre", "aventure", "author1", 50, "6.16", "addItemBook() add Book with wrong password");

        // check that 'sn' was not modified
        nbTests++;
        if (nbBooks != sn.nbBooks()) {
            System.out.println("Error : the number of Books was unexepectedly changed by addItemBook()");
            nbErrors++;
        }


        // Display final state of 'sn'
        System.out.println("Final state of the social network : " + sn);


        // Print a summary of the tests and return test results
        try{
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("addItemBookTest : " + tr);
            return tr;
        }
        catch (NotTestReportException e){ //This shouldn't happen
            System.out.println("Unexpected error in addItemBookTest test code - Can't return valuable test results");
            return null;
        }
    }



    /**
     * Launches test()
     * @param args not used
     */

    public static void main(String[] args) {

        test();
    }

}
