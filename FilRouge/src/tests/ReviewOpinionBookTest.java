package tests;

import exceptions.*;
import opinion.SocialNetworkPremium;
import opinion.ISocialNetworkPremium;



public class ReviewOpinionBookTest {

    private static int BadEntryExceptionTest(ISocialNetworkPremium sn, String login,
                                             String pwd, String loginCommentAdd , String commentAddOpinion, String title, float mark, String comment, String testId, String errorMessage) {
        try {
            sn.reviewOpinionBook(login, pwd, loginCommentAdd, commentAddOpinion,title ,mark,comment); // Try to add review on this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (BadEntryException e) { // BadEntry exception was thrown by

            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }

    private static int reviewOpinionBookOKTest(ISocialNetworkPremium sn, String login,
                                               String pwd, String loginCommentAdd, String commentAddOpinion, String title,float mark,String comment, String testId, String errorMessage) {

        try {
            sn.reviewOpinionBook(login, pwd, loginCommentAdd, commentAddOpinion, title , mark, comment); // Try to add this member
            // No exception was thrown. That's a good start !
            return 0; // return success code : everything is OK, nothing to
            // display
        } catch (Exception e) {// An exception was thrown by addMember() : this
            // is an error case
            System.out
                    .println("Err " + testId + " : unexpected exception " + e); // Error
            // message
            // displayed
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error code
        }
    }

    private static int NotMemberExceptionTest(ISocialNetworkPremium sn, String login,
                                              String pwd, String loginCommentAdd, String commentAddOpinion, String title,float mark,String comment, String testId, String errorMessage) {
        try {
            sn.reviewOpinionBook(login, pwd, loginCommentAdd, commentAddOpinion,title ,mark,comment); // Try to add this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotMemberException e) { // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }

    }

    private static int NotItemExceptionTest(ISocialNetworkPremium sn, String login,
                                            String pwd, String loginCommentAdd ,String commentAddOpinion, String title,float mark,String comment, String testId, String errorMessage) {
        try {
            sn.reviewOpinionBook(login, pwd,loginCommentAdd, commentAddOpinion,title ,mark,comment); // Try to add this Book comment
            // Reaching this point means that no exception was thrown by
            // reviewItemBook()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotItemException e) { // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            return 0;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }



    public static TestReport test(){

        ISocialNetworkPremium sn = new SocialNetworkPremium();


        int nbTests = 0; // total number of performed tests
        int nbErrors = 0; // total number of failed tests


        try {
            // Add member for testing
            sn.addMember("Paul", "12345", "SuperUser");
            sn.addMember("Nicholas", "45678", "SuperUser");
            sn.addMember("Thomas", "password", "SuperUser");


            // Add Book for testing
            sn.addItemBook("Paul", "12345", "American History X", "Thriller", "Patrick Timsit", 350);
            sn.addItemBook("Paul", "12345", "Lucky Luke", "BD", "Jean", 50);

            // Add Movie for testing
            sn.addItemFilm("Paul", "12345", "Nocturnal Animals", "Thriller", "Tom Ford", "Amy Adams",120);

            // Add Comment on Book for testing
            sn.reviewItemBook("Paul","12345","American History X", (float) 4.1,"C'est vraiment un bon Book (Paul)");
            sn.reviewItemBook("Nicholas","45678","American History X", (float) 2.1,"C'est vraiment un bon Book (Nicholas)");

            sn.reviewItemBook("Paul","12345","Lucky Luke", (float) 3.1,"Retour en enfance");
            sn.reviewItemBook("Nicholas","45678","Lucky Luke", (float) 1.1,"Amusant");

        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
        }

        System.out.println("Testing reviewOpinionBook()");


        // <=> test n°1

        // check if incorrect parameters cause reviewItemBook() cause BadEntryException
        // exception



        // Add review Opinion on Book American History X at Nicholas
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Paul", "12345", "Nicholas", "C'est vraiment un bon Book (Nicholas)", "American History X", (float) 2.1, "C'est vraiment un bon choix","10.1",
                "reviewOpinionBook didn't add correctly comment");
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Paul", "12345", "Nicholas", "C'est vraiment un bon Book (Nicholas)", "American History X", (float) 3.1, "C'est vraiment bien","10.3",
                "reviewOpinionBook didn't add correctly comment");
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Thomas", "password", "Nicholas", "C'est vraiment un bon Book (Nicholas)", "American History X", (float) 4.1, "C'est de la bombe","10.2",
                "reviewOpinionBook didn't add correctly comment");


        // Add review Opinion on Book American History X at Paul
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Nicholas", "45678", "Paul", "C'est vraiment un bon Book (Paul)", "American History X", (float) 2.1, "C'est vraiment un bon choix","10.4",
                "reviewOpinionBook didn't add correctly comment");
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Thomas", "password", "Paul", "C'est vraiment un bon Book (Paul)", "American History X", (float) 2.5, "C'est vraiment bien","10.3",
                "reviewOpinionBook didn't add correctly comment");

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////::

        // Add review Opinion on Book Lucky Luke at Nicholas
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Paul", "12345", "Nicholas", "Amusant", "Lucky Luke", (float) 2.1, "C'est vraiment un bon choix","10.6",
                "reviewOpinionBook didn't add correctly comment");
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Thomas", "password", "Nicholas", "Amusant", "Lucky Luke", (float) 3.1, "C'est vraiment bien","10.7",
                "reviewOpinionBook didn't add correctly comment");


        // Add review Opinion on Book Lucky Luke at Paul
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Nicholas", "45678", "Paul", "Retour en enfance", "Lucky Luke", (float) 2.1, "C'est vraiment un bon choix","10.8",
                "reviewOpinionBook didn't add correctly comment");
        nbTests++;
        nbErrors += reviewOpinionBookOKTest(sn, "Thomas", "password", "Paul", "Retour en enfance", "Lucky Luke", (float) 2.5, "C'est vraiment bien","10.9",
                "reviewOpinionBook didn't add correctly comment");

        ///// FIN TEST OK /////


        // Password minus 4 caractere

        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", " ","Nicholas", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, "C'est vraiment bien","10.10",
                "reviewOpinionBook doesn't take care of password");

        // Bad login
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Pa uL", "12345", "Nicholas", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, "C'est vraiment bien","10.11",
                "reviewOpinionBook doesn't take care of trailing blanks and leadings.");

        // Bad login
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "   ", "12345", "Nicholas", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, "C'est vraiment bien","10.11 bis",
                "reviewOpinionBook doesn't take care of trailing blanks and leadings.");


        // Bad login
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Pa uL", "12345", "   ", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, "C'est vraiment bien","10.11 ter",
                "reviewOpinionBook doesn't take care of trailing blanks and leadings.");


        // Mark outside the limit
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345","Nicholas",  "C'est vraiment un bon Book (commentaire)", "American History X", (float) 7.1, "C'est vraiment bien","10.12",
                "reviewOpinionBook doesn't take care of mark outside the limit");


        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paul", "12345","NicH olas",  "C'est vraiment un bon Book (commentaire)", "American History X", (float) 4.1, "C'est vraiment bien","10.13",
                "reviewOpinionBook doesn't take care of mark outside the limit");


        // Title Null
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345","Nicholas", "C'est vraiment un bon Book (commentaire)", null, (float) 3.1, "C'est vraiment bien","10.14",
                "reviewOpinionBook doesn't take care of title Null");

        // Without Comments
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, null,"10.15",
                "reviewOpinionBook doesn't take if there is comment or not");

        // Bad Password
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paul", "BadPassword", "Nicholas", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, "C'est vraiment bien","10.16",
                "reviewOpinionBook doesn't take care of bad password");

        // Good Password but bad login
        nbTests++;
        nbErrors += NotMemberExceptionTest(sn, "Paulo", "12345", "Nicholas", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, "C'est vraiment bien","10.17",
                "reviewOpinionBook doesn't take care of bad login and good password");

        // Movie instead of a Book
        nbTests++;
        nbErrors += NotItemExceptionTest(sn, "Paul", "12345","Nicholas",  "C'est vraiment un bon Book (commentaire)", "Nocturnal Animals", (float) 3.1, "C'est vraiment bien","10.18",
                "reviewOpinionBook doesn't take care of movie instead of Book");

        // Test commentaire de base Null
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas", null, "American History X", (float) 3.1, "C'est vraiment bien","10.19",
                "reviewOpinionBook doesn't take care if comment null");

        // Test commentaire Opinion null
        nbTests++;
        nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nicholas", "C'est vraiment un bon Book (commentaire)", "American History X", (float) 3.1, null,"10.20",
                "reviewOpinionBook doesn't take care if comment Opinion is null");


        // Display final state of 'sn'
        System.out.println("Final state of the social network : " + sn);

        // Print a summary of the tests and return test results
        try{
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("Review Opinion Book Test : " + tr);
            return tr;
        }
        catch (NotTestReportException e){ //This shouldn't happen
            System.out.println("Unexpected error in ReviewItemBook test code - Can't return valuable test results");
            return null;
        }

    }


    /**
     * Launches test()
     * @param args not used
     */
    public static void main(String[] args) {
        test();
    }

}
