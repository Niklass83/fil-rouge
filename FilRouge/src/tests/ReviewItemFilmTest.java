package tests;

import exceptions.BadEntryException;
import exceptions.NotItemException;
import exceptions.NotMemberException;
import exceptions.NotTestReportException;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;
import java.util.LinkedList;

public class ReviewItemFilmTest {

	/**
	 * @param sn
	 * @param login
	 * @param pwd
	 * @param title
	 * @param mark
	 * @param comment
	 * @param testId
	 * @param errorMessage
	 * @return value of the error
	 *
	 * With BadEntryExceptionTest we will test every kind of error and will return 0 if BadEntryException is thrown or 1 if the this exception
	 * isn't thrown
	 */
	private static int BadEntryExceptionTest(ISocialNetwork sn, String login,
                                             String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		try {
			sn.reviewItemFilm(login, pwd, title, mark,comment); // Try to add review on this film comment
			// Reaching this point means that no exception was thrown by
			// reviewItemFilm()
			System.out.println("Err " + testId + " : " + errorMessage); // display
																		// the
																		// error
																		// message
			return 1; // and return the "error" value
		} catch (BadEntryException e) { // BadEntry exception was thrown by
										
			return 0;
		}
		catch (Exception e) { // An exception was thrown by addMember(), but
						// it was not the expected exception
						// AlreadyExists
				System.out.println("Err " + testId + " : unexpected exception. "
				+ e); // Display a specific error message
				e.printStackTrace(); // Display contextual info about what happened
				return 1; // return error value
				}
	}

	/**
	 * @param sn
	 * @param login
	 * @param pwd
	 * @param title
	 * @param mark
	 * @param comment
	 * @param testId
	 * @param errorMessage
	 * @return the value of the error
	 *
	 * reviewItemFilmOKTest check if with good parameters we can create a new review on an already created Movie
	 *
	 */
	private static int reviewItemFilmOKTest(ISocialNetwork sn, String login,
                                            String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		
		try {
			float test = sn.reviewItemFilm(login, pwd, title, mark, comment); // Try to add this member
			// No exception was thrown. That's a good start !
			if (test == -1) { // But the number of members
													// hasn't changed
													// accordingly
				System.out.println("Err " + testId); // Error message displayed
				return 1; // return error code
			} else
				return 0; // return success code : everything is OK, nothing to
							// display
		} catch (Exception e) {// An exception was thrown by addMember() : this
								// is an error case
			System.out
					.println("Err " + testId + " : unexpected exception " + e); // Error
																				// message
																				// displayed
			e.printStackTrace(); // Display contextual info about what happened
			return 1; // return error code
		}
	}


	/**
	 * @param sn
	 * @param login
	 * @param pwd
	 * @param title
	 * @param mark
	 * @param comment
	 * @param testId
	 * @param errorMessage
	 * @return the value of the error
	 *
	 * With NotMemberExceptionTest we will test every kind of error and will return 0 if NotMemberException is thrown or 1 if the this exception
	 * isn't thrown
	 *
	 */
	private static int NotMemberExceptionTest(ISocialNetwork sn, String login,
                                              String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		try {
			sn.reviewItemFilm(login, pwd, title, mark,comment); // Try to add this film comment
			// Reaching this point means that no exception was thrown by
			// reviewItemFilm()
			System.out.println("Err " + testId + " : " + errorMessage); // display
																		// the
																		// error
																		// message
			return 1; // and return the "error" value
		} catch (NotMemberException e) { // BadEntry exception was thrown by
										// addMember() : this is a good start!
										// Let's now check if 'sn' was not
										// impacted
			return 0;
		}
			catch (Exception e) { // An exception was thrown by addMember(), but
				// it was not the expected exception
				// AlreadyExists
				System.out.println("Err " + testId + " : unexpected exception. "
				+ e); // Display a specific error message
				e.printStackTrace(); // Display contextual info about what happened
				return 1; // return error value
				}
		
	}

	/**
	 * @param sn
	 * @param login
	 * @param pwd
	 * @param title
	 * @param mark
	 * @param comment
	 * @param testId
	 * @param errorMessage
	 * @return the value of the error
	 *
	 * With NotItemExceptionTest we will test every kind of error and will return 0 if NotItemException is thrown or 1 if the this exception
	 * isn't thrown
	 *
	 */
	private static int NotItemExceptionTest(ISocialNetwork sn, String login,
                                            String pwd, String title, float mark, String comment, String testId, String errorMessage) {
		try {
			sn.reviewItemFilm(login, pwd, title, mark,comment); // Try to add this film comment
			// Reaching this point means that no exception was thrown by
			// reviewItemFilm()
			System.out.println("Err " + testId + " : " + errorMessage); // display
																		// the
																		// error
																		// message
			return 1; // and return the "error" value
		} catch (NotItemException e) { // BadEntry exception was thrown by
										// addMember() : this is a good start!
										// Let's now check if 'sn' was not
										// impacted
			return 0;
		}
			catch (Exception e) { // An exception was thrown by addMember(), but
				// it was not the expected exception
				// AlreadyExists
				System.out.println("Err " + testId + " : unexpected exception. "
				+ e); // Display a specific error message
				e.printStackTrace(); // Display contextual info about what happened
				return 1; // return error value
				}
	}

	/**
	 * @return TestReport
	 *
	 * In this method we will test all the different kind of Exception we want to throw
	 * At the end we will have the number of test we made and the number of error we made
	 * Like that we could check if our program is good or not.
	 *
	 */
	public static TestReport test(){

		ISocialNetwork sn = new SocialNetwork();


		int nbTests = 0; // total number of performed tests
		int nbErrors = 0; // total number of failed tests
		
		
		try {
		// Add member for testing
		sn.addMember("Paul", "12345", "SuperUser");
		sn.addMember("Nicholas", "45678", "SuperUser");
		
		// Add Movie for testing
		sn.addItemFilm("Paul", "12345", "Nocturnal Animals", "Thriller", "Tom Ford", "Amy Adams",120);
		sn.addItemFilm("Paul", "12345", "La Grande Vadrouille", "Comedie", "Jean Jacques", "Funes",120);
		
		// Add Book for testing
		sn.addItemBook("Paul", "12345", "American History X", "Thriller", "Patrick Timsit", 350);
		
		}
		catch (Exception e) { // An exception was thrown by addMember(), but
			// it was not the expected exception
			// AlreadyExists
			System.out.println("Err " + " : unexpected exception. "
			+ e); // Display a specific error message
			e.printStackTrace(); // Display contextual info about what happened
			}

		System.out.println("Testing reviewItemFilm()");
		

		// <=> test n°1

		// check if incorrect parameters cause reviewItemFilm() cause BadEntryException
		// exception
		

		nbTests++;
		nbErrors += reviewItemFilmOKTest(sn, "Paul", "12345", "Nocturnal Animals", (float) 1,"C'est vraiment un bon film (commentaire)","4.1",
				"reviewItemFilm add correctly comment");


		nbTests++;
		nbErrors += reviewItemFilmOKTest(sn, "Nicholas", "45678", "Nocturnal Animals", (float) 4,"Best Movie Ever","4.3",
				"reviewItemFilm add correctly comment with last member");

		nbTests++;
		nbErrors += reviewItemFilmOKTest(sn, "Paul", "12345", "Nocturnal animals", (float) 2,"Super film (commentaire)","4.2",
				"reviewItemFilm change correctly comment");
		


		// Password minus 4 caractere
		
		nbTests++;
		nbErrors += BadEntryExceptionTest(sn, "Paul", " ", "Nocturnal Animals", (float) 1.1,"C'est vraiment un bon film (commentaire)","4.4",
				"reviewItemFilm doesn't take care of password");
		
		// Bad login
		nbTests++;
		nbErrors += NotMemberExceptionTest(sn, "Pa uL", "12345", "Nocturnal Animals", (float) 1.1,"C'est vraiment un bon film (commentaire)","4.5",
				"reviewItemFilm doesn't take care of trailing blanks and leadings.");

		// Bad login only trailing blanks
		nbTests++;
		nbErrors += BadEntryExceptionTest(sn, "   ", "12345", "Nocturnal Animals", (float) 1.1,"C'est vraiment un bon film (commentaire)","4.5bis",
				"reviewItemFilm doesn't take care of trailing blanks and leadings.");
		
		// Mark outside the limit
		nbTests++;
		nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nocturnal Animals", (float) 6.5,"C'est vraiment un bon film (commentaire)","4.7",
				"reviewItemFilm doesn't take care of mark outside the limit");

		// Title Null
		nbTests++;
		nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", null, (float) 4.5,"C'est vraiment un bon Book (commentaire)","4.6",
				"reviewItemBook doesn't take care of title Null");
		
		// Without Comments
		nbTests++;
		nbErrors += BadEntryExceptionTest(sn, "Paul", "12345", "Nocturnal Animals", (float) 1.1,null,"4.8",
				"reviewItemFilm doesn't take if there is comment or not");
		
		// Bad Password
		nbTests++;
		nbErrors += NotMemberExceptionTest(sn, "Paul", "BadPassword", "Nocturnal Animals", (float) 1.1,"C'est vraiment un bon film (commentaire)","4.9",
				"reviewItemFilm doesn't take care of bad password");
		
		// Good Password but bad login
		nbTests++;
		nbErrors += NotMemberExceptionTest(sn, "Paulo", "12345", "Nocturnal Animals", (float) 1.1,"C'est vraiment un bon film (commentaire)","4.10",
				"reviewItemFilm doesn't take care of bad login and good password");
		
		// Book instead of a movie
		nbTests++;
		nbErrors += NotItemExceptionTest(sn, "Paul", "12345", "American History X", (float) 1.1,"C'est vraiment un bon film (commentaire)","4.11",
				"reviewItemFilm doesn't take care of book instead of movie");

		// Test Consult Item Film
		// Doit afficher [Nocturnal Animals, [Commentaire : Super film (commentaire) Notes : 1.1, Commentaire : Best Movie Ever Notes : 1.1]]

		nbTests++;
		LinkedList<String> testConsult = new LinkedList<String>();
		try {
			testConsult = sn.consultItems("Nocturnal Animals");
			System.out.println(testConsult.toString());
		}
		catch (Exception e) { // An exception was thrown by addMember(), but
			// it was not the expected exception
			// AlreadyExists
			System.out.println("Err " + " : unexpected exception. "
					+ e); // Display a specific error message
			e.printStackTrace(); // Display contextual info about what happened
		}


		// Display final state of 'sn'
		System.out.println("Final state of the social network : " + sn);
		
		// Print a summary of the tests and return test results
				try{
					TestReport tr = new TestReport(nbTests, nbErrors);	
					System.out.println("ReviewItemFilm : " + tr);
					return tr;	
				}
				catch (NotTestReportException e){ //This shouldn't happen
					System.out.println("Unexpected error in ReviewItemFilm test code - Can't return valuable test results");
					return null;
					}

	}
	
	
	/**
	 * Launches test()
	 * @param args not used
	 */
	public static void main(String[] args) {
		test();
	}
}
