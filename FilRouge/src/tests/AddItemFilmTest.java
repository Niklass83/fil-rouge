package tests;

import exceptions.BadEntryException;
import exceptions.ItemFilmAlreadyExistsException;
import exceptions.NotMemberException;
import exceptions.NotTestReportException;
import opinion.ISocialNetwork;
import opinion.SocialNetwork;

public class AddItemFilmTest {


    /**
     * @param sn           socialNetwork to test
     * @param login        a member from the socialNtework
     * @param password     a member from the socialNetwork
     * @param title        of the film to add for the test
     * @param kind         of the film to add for the test
     * @param director     of the film to add for the test
     * @param scenarist    of the film to add for the test
     * @param duration     of the film to add for the test
     * @param testId       of the test we try
     * @param errorMessage to display if test isn't OK
     *
     * @return an int to increment the nbTotalTests
     *
     *      Test the function addItemFilm with bad parameters for the book to add and watch if the error is the expected one.
     */
    private static int addItemFilmBadEntryTest(ISocialNetwork sn, String login,
                                               String password, String title, String kind, String director, String scenarist, int duration, String testId, String errorMessage) {

        int nbFilms = sn.nbFilms(); // Number of Items Film when starting to
        // run this method
        try {
            //sn.addMember(login, password, profile); // Try to add this member
            // Reaching this point means that no exception was thrown by
            // addMember()

            sn.addItemFilm(login, password, title, kind, director, scenarist, duration);

            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (BadEntryException e) { // BadEntry exception was thrown by
            // addMember() : this is a good start!
            // Let's now check if 'sn' was not
            // impacted
            if (sn.nbFilms() != nbFilms) { // The number of members has
                // changed : this is an error
                // case.
                System.out
                        .println("Err "
                                + testId
                                + " : BadEntry was thrown but the number of films was changed"); // Display
                // a
                // specific
                // error
                // message
                return 1; // return "error" value
            } else
                // The number of members hasn't changed, which is considered a
                // good indicator that 'sn' was not modified
                return 0; // return success value : everything seems OK, nothing
            // to display
        } catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception BadEntry
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }


    /**
     * @param sn           socialNetwork to test
     * @param login        a member from the socialNtework
     * @param password     a member from the socialNetwork
     * @param title        of the film to add for the test
     * @param kind         of the film to add for the test
     * @param director     of the film to add for the test
     * @param scenarist    of the film to add for the test
     * @param duration     of the film to add for the test
     * @param testId       of the test we try
     * @param errorMessage to display if test isn't OK
     *
     * @return an int to increment the nbTotalTests
     *
     *      Test the function addItemFilm with bad parameters for the book to add (already in the list) and watch if the error is the expected one.
     */
    private static int addItemFilmAlreadyExistsTest(ISocialNetwork sn, String login,
                                                    String password, String title, String kind, String director, String scenarist, int duration, String testId, String errorMessage) {
        int nbFilms = sn.nbFilms(); // Number of members when starting to
        // process this method
        try {

            sn.addItemFilm(login, password, title, kind, director, scenarist, duration); // Try to add this film
            // Reaching this point means that no exception was thrown by
            // addItemFilm()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (ItemFilmAlreadyExistsException e) {// AlreadyExists exception was
            // thrown by addItemFilms () :
            // this is a good start!
            // Let's now check if 'sn'
            // was not impacted
            if (sn.nbFilms() != nbFilms) {
                System.out.println("Err "+ testId+ " : FilmAlreadyExists was thrown, but the number of members was changed"); // Display
                // a
                // specific
                // error
                // message
                return 1;// and return the "error" value
            } else
                return 0; // return success value : everything is OK, nothing to
            // display
        } catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }

    /**
     * @param sn           socialNetwork to test
     * @param login        a member from the socialNtework
     * @param password     a member from the socialNetwork
     * @param title        of the film to add for the test
     * @param kind         of the film to add for the test
     * @param director     of the film to add for the test
     * @param scenarist    of the film to add for the test
     * @param duration     of the film to add for the test
     * @param testId       of the test we try
     * @param errorMessage to display if test isn't OK
     *
     * @return an int to increment the nbTotalTests
     *
     *      Test the function addItemFilm with bad parameters for the member (auth failed) to add and watch if the error is the expected one.
     */
    private static int addItemFilmNotMemberException(ISocialNetwork sn, String login,
                                                     String password, String title, String kind, String director, String scenarist, int duration, String testId, String errorMessage) {

        int nbFilms = sn.nbFilms();                         // process this method
        try {
            sn.addItemFilm(login, password, title, kind, director, scenarist, duration); // Try to add this member
            // Reaching this point means that no exception was thrown by
            // addMember()
            System.out.println("Err " + testId + " : " + errorMessage); // display
            // the
            // error
            // message
            return 1; // and return the "error" value
        } catch (NotMemberException e) {// AlreadyExists exception was
            // thrown by addMember() :
            // this is a good start!
            // Let's nprofileow check if 'sn'
            // was not impacted
            if (sn.nbFilms() != nbFilms) {
                System.out
                        .println("Err "
                                + testId
                                + " : MemberAlreadyExists was thrown, but the number of members was changed"); // Display
                // a
                // specific
                // error
                // message
                return 1;// and return the "error" value
            } else
                return 0; // return success value : everything is OK, nothing to
            // display
        } catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + testId + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
            return 1; // return error value
        }
    }

    /**
     * @return the results of tests by displaying them
     */
    public static TestReport test(){

        ISocialNetwork sn = new SocialNetwork();

        // number of books in 'sn' (should be 0
        // here)
        int nbFilms = sn.nbFilms(); // number of films in 'sn' (should be 0
        // here)

        int nbTests = 0; // total number of performed tests
        int nbErrors = 0; // total number of failed tests
        try {
            // Add member for testing
            sn.addMember("Antoine", "qsdfgh","grand amoureux" );
            sn.addItemFilm("Antoine", "qsdfgh", "title", "adventure", "director1", "scenarist", 1);
            nbFilms++;
        }
        catch (Exception e) { // An exception was thrown by addMember(), but
            // it was not the expected exception
            // AlreadyExists
            System.out.println("Err " + " : unexpected exception. "
                    + e); // Display a specific error message
            e.printStackTrace(); // Display contextual info about what happened
        }

        System.out.println("Testing addItemFilm()");

        //3
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, null, "qsdfgh", "title", "adventure", "director1", "scenarist", 1, "3", "addItemFilm() doesn't reject null pseudo");
        //4
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, " ", "qsdfgh", "title", "adventure", "director1", "scenarist", 1, "4", "addItemFilm() doesn't reject logins that don't contain at least one character other than space");
        //5
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, "Antoine", null, "title", "adventure", "director1", "scenarist", 1, "5", "addItemFilm() doesn't reject null password");
        //6
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, "Antoine", "qsd", "title", "adventure", "director1", "scenarist", 1, "6", "addItemFilm() doesn't reject passwords that don't contain at least 4 characters (not taking into account leading or trailing blanks)");
        //7
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, "Antoine", "qsdfgh", "title", null, "director1", "scenarist", 1, "7", "addItemFilm() doesn't reject null kind");
        //8
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, "Antoine", "qsdfgh", "title", "adventure", null, "scenarist", 1, "8", "addItemFilm() doesn't reject null director");
        //9
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, "Antoine", "qsdfgh", "title", "adventure", "director1", null, 1, "9", "addItemFilm() doesn't reject null scenarist");
        //10
        nbTests++;
        nbErrors += addItemFilmBadEntryTest(sn, "Antoine", "qsdfgh", "title", "adventure", "director1", "scenarist", -1, "10", "addItemFilm() doesn't reject negative duration");
        //11
        nbTests++;
        nbErrors += addItemFilmAlreadyExistsTest(sn, "Antoine", "qsdfgh", "title", "adventure", "director1", "scenarist", 1, "11", "addItemFilm() add a film already in list (first place)");
        //12
        nbTests++;
        nbErrors += addItemFilmAlreadyExistsTest(sn, "Antoine", "qsdfgh", "title", "adventure", "director1", "scenarist", 1, "12", "addItemFilm() add a film already in list (last place)");
        //13
        nbTests++;
        nbErrors += addItemFilmAlreadyExistsTest(sn, "Antoine", "qsdfgh", "tITle", "adventure", "director1", "scenarist", 1, "13", "addItemFilm() add a film already in list (casse sensitive)");
        //14
        nbTests++;
        nbErrors += addItemFilmAlreadyExistsTest(sn, "Antoine", "qsdfgh", "title ", "adventure", "director1", "scenarist", 1, "14", "addItemFilm() add a film already in list (blank)");
        //15
        nbTests++;
        nbErrors += addItemFilmNotMemberException(sn, "AnT oINe", "qsdfgh", "title123", "adventure", "director1", "scenarist", 1, "15", "addItemFilm() add a film with the same user with different case");
        //16
        nbTests++;
        nbErrors += addItemFilmNotMemberException(sn, "Martine", "qsdfgh", "title1123", "adventure", "director1", "scenarist", 1, "16", "addItemFilm() add by a non-user");
        //17
        nbTests++;
        nbErrors += addItemFilmNotMemberException(sn, "Antoine", "qsdfgazeazh", "titl21e123", "adventure", "director1", "scenarist", 1, "17", "addItemFilm() add film with wrong password");






        // check that 'sn' was not modified
        nbTests++;
        if (nbFilms != sn.nbFilms()) {
            System.out.println("Error : the number of films was unexepectedly changed by addItemFilm()");
            nbErrors++;
        }


        // Display final state of 'sn'
        System.out.println("Final state of the social network : " + sn);


        // Print a summary of the tests and return test results
        try{
            TestReport tr = new TestReport(nbTests, nbErrors);
            System.out.println("addItemFilmTest : " + tr);
            return tr;
        }
        catch (NotTestReportException e){ //This shouldn't happen
            System.out.println("Unexpected error in addItemFilmTest test code - Can't return valuable test results");
            return null;
        }
    }


    /**
     * Launches test()
     * @param args not used
     */

    public static void main(String[] args) {

        test();
    }

}
