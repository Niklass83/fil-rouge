package opinion;

import exceptions.BadEntryException;

import java.util.LinkedList;

public class ItemBook {
    private String title;
    private String kind;
    private String author;
    private int    nbPages;
    private float  meanMark=0f;

    protected LinkedList<Opinion> theOpinions = new LinkedList<Opinion>();

    /**
     * @param title   of the ItemBook to creat
     * @param kind    of the ItemBook to creat
     * @param author  of the ItemBook to creat
     * @param nbPages of the ItemBook to creat
     *
     * @throws BadEntryException
    ItemBook is a constructor which tests the parameters given and creat an ItemBook if they're OK
     */

    public ItemBook(String title, String kind, String author, int nbPages) throws BadEntryException{
        // testing entry parameters
        if(title == null) {
            throw new BadEntryException("Error : Title null");
        }else if(title.replace(" ", "").length()<=1) {
            throw new BadEntryException("Error : Title more trailing blanks");
        }else if(kind == null) {
            throw new BadEntryException("Error : Kind null");
        }else if(author == null) {
            throw new BadEntryException("Error : Author null");
        }else if(nbPages<=0){
            throw new BadEntryException("Error : nbPages must be strictly positive");
        }
        this.author  = author;
        this.kind    = kind;
        this.title   = title;
        this.nbPages = nbPages;

    }

    /**
     * @return the the list of opinions for an ItemBook
     */
    public LinkedList<Opinion> getList(){
        return theOpinions;
    }

    /**
     * @return the mean Mark of the ItemBook in parameters
     */

    public float getMeanMark(){ return this.meanMark; }


    /**
     * @param title of the ItemBook to test
     * @return true if the book is already in the list
     */
    public boolean isIt (String title) {

        return (this.title.trim().equalsIgnoreCase(title.trim()));
    }
    /**
     * @param comment comment given by a user
     * @param mark    mark given by a user
     * @param member  user that give these two parameters
     *
     * @return the mean mark of the ItemBook
     *
     * @throws BadEntryException
     *
     * addOpinion checks if the user is a registered member of social-network
     * if so, it tests if the member already add a comment and a mark :
     * 	- if not, it add them
     * 	- if so, it erase the first comment and place the new one
     */

    public float addOpinion(String comment, float mark, Member member) throws BadEntryException {
        Opinion opinionTest = new Opinion(comment, mark, member);
        float sum=0;
        float sumKarma=0f;
        float karmaMember;

        if(theOpinions.size()>=1){
            sumKarma=1f;
        }

        boolean testUpdate=false;

        for (Opinion opinion : theOpinions) {
            karmaMember=opinion.member.getKarma();
            if (opinion.member.areYou(member.getLogin())) {
                float oldMark = opinion.update(opinionTest.getComment(),opinionTest.getMark());
                sum+=mark * karmaMember;
                testUpdate=true;
            }else{
                sum += opinion.getMark() * karmaMember;
            }
        }

        if(testUpdate){
            return this.meanMark = meanBook(sum,sumKarma+member.getKarma());
        }
        else {
            theOpinions.add(opinionTest);
            sum += mark * member.getKarma();
            return this.meanMark = meanBook(sum, sumKarma + member.getKarma());
        }
    }
    /**
     * @return the mean for a Karma
     */
    public float meanBook(float sum, float sumKarma){
        float mean = 1f;
        return mean = sum/sumKarma;
    }
    /**
     * @param memberAlreadyWriteOpinion login of the member who wants to add a review
     * @param commentAddOpinion         comment on wich we had a review
     * @param mark                      mark given by a user
     * @param comment                   comment given by a user
     * @param memberAddReviewOpinion    user that give these parameters
     *
     * @throws BadEntryException
     *
     * addReviewOpinion checks if the user is a registered member of social-network and if the opinion wich we have to comment exists
     * if so, it calculate the mark considering karma.
     */

    public void addReviewOpinion(Member memberAlreadyWriteOpinion, String commentAddOpinion, float mark, String comment, Member memberAddReviewOpinion) throws BadEntryException{
        if(commentAddOpinion == null){
            throw new BadEntryException("Error : Bad Comment");
        }
        boolean testOpinion = false;
        for(Opinion opinion : theOpinions){
            if(opinion.isItOpinionReview(memberAlreadyWriteOpinion.getLogin(),commentAddOpinion)){
                memberAlreadyWriteOpinion.setKarma(opinion.reviewOpinion(comment,mark,memberAddReviewOpinion));
                testOpinion=true;
            }
        }
        if(!testOpinion) {
            throw new BadEntryException("Error : Not in the Opinion List");
        }

        float sum=0;
        float sumKarma=0;
        float karmaMember;

        for (Opinion opinionSum : theOpinions ) {
            karmaMember=opinionSum.member.getKarma();
            sum+=opinionSum.getMark()*karmaMember;
            sumKarma+=karmaMember;
        }

        this.meanMark=meanBook(sum,sumKarma);
    }

    public String toString(){
        return this.title;
    }

}
