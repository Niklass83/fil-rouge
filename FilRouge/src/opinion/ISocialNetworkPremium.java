package opinion;

import exceptions.*;

public interface ISocialNetworkPremium extends ISocialNetwork {



    public void reviewOpinionFilm(String login, String pwd, String commentAddOpinion, String loginCommentAdd, String title, float mark, String comment) throws BadEntryException, NotMemberException, NotItemException;
    public void reviewOpinionBook(String login, String pwd, String commentAddOpinion, String loginCommentAdd, String title, float mark, String comment) throws BadEntryException, NotMemberException, NotItemException;

}
