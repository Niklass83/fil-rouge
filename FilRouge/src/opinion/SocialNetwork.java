//Tet2
package opinion;

import java.util.LinkedList;

import exceptions.*;

public class SocialNetwork implements ISocialNetwork {

	protected LinkedList<Member>   theMembers = new LinkedList<Member>();
	protected LinkedList<ItemFilm> theFilms   = new LinkedList<ItemFilm>();
	protected LinkedList<ItemBook> theBooks   = new LinkedList<ItemBook>();

	@Override
	public int nbMembers() {
		return theMembers.size();
	}

	@Override
	public int nbFilms() {
		     return theFilms.size();
	}

	@Override
	public int nbBooks() { return theBooks.size();  }

	/**
	 * @param login    the new member's login
	 * @param password the new member's password
	 * @param profile  a free String describing the new member's profile
	 * @throws BadEntryException
	 * @throws MemberAlreadyExistsException
	 */
	@Override
	public void addMember(String login, String password, String profile)
			throws BadEntryException, MemberAlreadyExistsException {
		Member M1= new Member(login,password,profile);
		for (Member member : theMembers) {
			if(member.areYou(login)) {
				throw new MemberAlreadyExistsException("Error : Member Already Exist");
			}
		}

		theMembers.add(M1);

	}

	/**
	 * @param login        login of the member adding the film
	 * @param password     password of the member adding the film
	 * @param title        the new film's title
	 * @param kind         the new film's kind (adventure, thriller, etc.)
	 * @param director     the new film's director
	 * @param scriptwriter the new film's scriptwriter
	 * @param duration     the new film's duration (in minutes)
	 * @throws BadEntryException
	 * @throws NotMemberException
	 * @throws ItemFilmAlreadyExistsException
	 */
	@Override
	public void addItemFilm(String login, String password, String title,
							String kind, String director, String scriptwriter, int duration)
			throws BadEntryException, NotMemberException,
			ItemFilmAlreadyExistsException {
		Member m1 = new Member(login, password);
		boolean testId=false;
		for (Member member : theMembers) {
			if(member.areYou(m1.getLogin())) {
				if(member.getPassword().equals(m1.getPassword())) {
					testId=true;
					break;
				}
				else {
					throw new NotMemberException("Error : Not Authentified");
				}
			}
		}
		if(testId==false) {
			throw new NotMemberException("Error : Not Authentified");
		}
		// Creation Film pour verification si film est bien dans la liste
		ItemFilm filmTeste = new ItemFilm(title, kind, director, scriptwriter, duration);

		for (ItemFilm filmTest : theFilms) {
			if(filmTest.isIt(title)) {
				throw new ItemFilmAlreadyExistsException("Error : Film already exist");
			}
		}
		theFilms.add(filmTeste);
	}


	/**
	 * @param login    login of the member adding the book
	 * @param password password of the member adding the book
	 * @param title    the new book's title
	 * @param kind     the new book's kind (adventure, thriller, etc.)
	 * @param author   the new book's author
	 * @param nbPages  number of pages of the new book's
	 * @throws BadEntryException
	 * @throws NotMemberException
	 * @throws ItemBookAlreadyExistsException
	 */
	@Override
	public void addItemBook(String login, String password, String title,
							String kind, String author, int nbPages) throws BadEntryException,
			NotMemberException, ItemBookAlreadyExistsException {

		Member m1 = new Member(login, password);
		boolean testId=false;
		for (Member member : theMembers) {
			if(member.areYou(m1.getLogin())) {
				if(member.getPassword().equals(m1.getPassword())) {
					testId=true;
					break;
				}
				else {
					throw new NotMemberException("Error : Not Authentified");
				}
			}
		}
		if(testId==false) {
			throw new NotMemberException("Error : Not Authentified");
		}

		// Creation Book pour verification si film est bien dans la liste

		ItemBook bookTeste = new ItemBook(title, kind, author, nbPages);

		for (ItemBook bookTest : theBooks) {
			if(bookTest.isIt(title)) {
				throw new ItemBookAlreadyExistsException("Error : Book already exist");
			}
		}
		theBooks.add(bookTeste);
//
	}

	/**
	 * @param login    login of the member adding the review
	 * @param password password of the member adding the review
	 * @param title    the reviewed film's title
	 * @param mark     the mark given by the member for this film
	 * @param comment  the comment given by the member for this film
	 * @return add the review to the item Film
	 * @throws BadEntryException
	 * @throws NotMemberException
	 * @throws NotItemException
	 */
	@Override
	public float reviewItemFilm(String login, String password, String title,
								float mark, String comment) throws BadEntryException,
			NotMemberException, NotItemException {
		//check if parameter title is not NULL
		if(title==null){
			throw new BadEntryException("Error : Title Null");
		}
		else if(title.replace(" ","").length()<=1){
			throw new BadEntryException("Error : Title Only Trailing Blanks");
		}
		Member m1 = new Member(login, password);
		Member M2 = new Member();
		boolean testId=false;
		//member authentication (if present in the list of members
		for (Member member : theMembers) {
			if(member.auth(m1)) {
				M2=member;
				testId=true;
				break;
			}
		}
		if(testId==false) {
			throw new NotMemberException("Error : Not Authentified");
		}


		///verification if comment already written
		for (ItemFilm movie : theFilms) {
			if(movie.isIt(title)) {
				return movie.addOpinion(comment,mark,M2);
			}
		}
		throw new NotItemException("Error : Not a Movie");

	}

	/**
	 * @param login    login of the member adding the review
	 * @param password password of the member adding the review
	 * @param title    the reviewed book's title
	 * @param mark     the mark given by the member for this book
	 * @param comment  the comment given by the member for this book
	 * @return add the review to the item Book
	 * @throws BadEntryException
	 * @throws NotMemberException
	 * @throws NotItemException
	 */
	@Override

	public float reviewItemBook(String login, String password, String title,
								float mark, String comment) throws BadEntryException,
			NotMemberException, NotItemException {
		//check if parameter title is not NULL
		if(title==null){
			throw new BadEntryException("Error : Title Null");
		}
		else if(title.replace(" ","").length()<=1){
			throw new BadEntryException("Error : Title Only Trailing Blanks");
		}
		Member m1 = new Member(login, password);
		Member M2 = new Member();
		boolean testId=false;
		for (Member member : theMembers) {
			if(member.auth(m1)) {
				M2=member;
				testId=true;
				break;
			}
		}

		if(testId==false) {
			throw new NotMemberException("Error : Not Authentified");
		}
		///verification if comment already written
		for (ItemBook book : theBooks) {
			if(book.isIt(title)) {
				return book.addOpinion(comment,mark,M2);
			}
		}
		throw new NotItemException("Error : Not a Book");
	}


	/**
	 * @param title title of searched item(s)
	 * @return the Opinion
	 * @throws BadEntryException
	 */
	@Override
	public LinkedList<String> consultItems(String title)throws BadEntryException {
		LinkedList<String> Opinion = new LinkedList<String>();
		if(title == null) {
			throw new BadEntryException("Error : Title null");
		}
		else if(title.replace(" ", "").length()<=1) {
			throw new BadEntryException("Error : Title more trailing blanks");
		}
		Opinion.add("");
		for (ItemFilm Film :theFilms){
			if(Film.isIt(title)){
				Opinion.add("Film : "+ Film.getList().toString() + " Moyenne : "+ Film.getMeanMark());
                Opinion.addFirst(title);
			}
		}
		for (ItemBook book :theBooks){
			if(book.isIt(title)){
				Opinion.add("Livre : "+ book.getList().toString()+ " Moyenne : "+ book.getMeanMark());
				Opinion.addFirst(title);
			}
		}

		return Opinion;
	}

	public String toString(){
		String socialNetwork = "members : " + theMembers.toString() + "\nFilms : " + theFilms.toString() +"\n";
		for (ItemFilm Film :theFilms){
			socialNetwork+=Film + "\t"+ Film.getList().toString() + " Moyenne : " + Film.getMeanMark()+"\n";


		}
		socialNetwork+="Books : "+theBooks.toString() +"\n";
		for (ItemBook Book :theBooks){
			socialNetwork += Book + "\t"+ Book.getList().toString() + " Moyenne : "+ Book.getMeanMark()+"\n";

		}

		return socialNetwork;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
