package opinion;

import exceptions.BadEntryException;

import java.util.LinkedList;

public class Opinion {
	private String comment;
	private float  mark;
	Member member;
	protected LinkedList<Opinion> theReviewOpinions= new LinkedList<Opinion>();

	/**
	 * @param comment to add
	 * @param mark    to add
	 * @param member  who wants to add an opinion
	 * @throws BadEntryException
	 *
	 * Opinion is an opinion's constructor who tests the parameters given before creat an opinion
	 */


	public Opinion(String comment, float mark,Member member) throws BadEntryException{
		if (comment==null) {
			throw new BadEntryException("Error : Bad Comment");
		}
		if((mark < 0) || (mark > 5) ) {
			throw new BadEntryException("Error : Bad Mark");
		}
		this.comment = comment;
		this.mark    = mark;
		this.member  = member;
	}

	/**
	 * @return the mark of the opinion given
	 */
	public float getMark() {
		return this.mark;
	}

	/**
	 * @return the comment of the opinion given
	 */
	public String getComment() {
		return this.comment;
	}

	/**
	 * @param comment to update
	 * @param mark    to update
	 * update allow the user to update in the list by modify it
	 */
	public float update(String comment,float mark) {
		float oldMark = this.mark;
		this.comment  = comment;
		this.mark     = mark;
		return oldMark;
	}
    /**
     * @param comment           comment to add on the opinion
     * @param mark              to add
     * @param memberAddOpinion  who add this review
     *
     * reviewOpinion try to add a new review for an opinion after testing we it's not a duplicate entry and if all parameters are OK
     *
     * @return the mean of comment on the opinion
     */
	public float reviewOpinion(String comment, float mark, Member memberAddOpinion) throws BadEntryException{
		float meanMark=0f;
		if(comment==null){
			throw new BadEntryException("Error : Bad comment in review opinion");
		}
		Opinion reviewOpinionTest = new Opinion(comment, mark, memberAddOpinion);
		float sum=0;
		for (Opinion opinionSum : theReviewOpinions ) {sum+=opinionSum.getMark();}
		for (Opinion opinion : theReviewOpinions) {
			if (opinion.isItOpinionReview(memberAddOpinion.getLogin(),comment)) {
				float oldMark=opinion.update(reviewOpinionTest.getComment(),reviewOpinionTest.getMark());
				sum-=oldMark;
				return meanMark=(sum+mark)/(theReviewOpinions.size());
			}
		}
		theReviewOpinions.add(reviewOpinionTest);
		return meanMark=((sum+mark)/(theReviewOpinions.size()));
	}
	/**
	 * @param comment         the opinion to test (comment)
	 * @param loginCommentAdd login of the user who entered this opinion
	 *
	 * @return true if the parameters entered match with an already existing review
	 */
	public boolean isItOpinionReview (String loginCommentAdd, String comment) {
		return (this.comment.trim().equalsIgnoreCase(comment.trim()) && this.member.getLogin().trim().equalsIgnoreCase(loginCommentAdd.trim()));
	}

	/**
	 * @param comment the opinion to test (comment)
	 *
	 * @return true if the parameters entered match with an already existing review
	 */
	public boolean isItOpinion (String comment) {
		return (this.comment.trim().equalsIgnoreCase(comment.trim()));
	}

	/**
	 * @return an opinion to a string format
	 */
	public String toStringReviewOpinion(){
		String reviewOpinion = "Commentaires de l'avis : ";
		for(Opinion opinion :theReviewOpinions){
			reviewOpinion +=" Commentaire : " + opinion.getComment() + "Note : " + opinion.getMark();
		}
		return reviewOpinion;
	}

	public String toString(){
		return "Commentaire : " +this.comment + " Notes : " +this.mark ;
	}
}
