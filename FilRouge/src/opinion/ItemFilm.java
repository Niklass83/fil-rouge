package opinion;

import java.util.LinkedList;
import exceptions.BadEntryException;

public class ItemFilm {

	private String title;
	private String kind;
	private String director;
	private String scriptwriter;
	private int    duration;
	private float  meanMark = 0f;

	protected LinkedList<Opinion> theOpinions = new LinkedList<Opinion>();
	/**
	 * @param title        of the film to creat
	 * @param kind         of the film to creat
	 * @param director     of the film to creat
	 * @param scriptwriter of the film to creat
	 * @param duration     of the film to creat
	 * @throws BadEntryException
	 *
	 * ItemFilm is a constructor which tests the parameters given and creat an ItemFilm if they're OK
	 */

	public ItemFilm(String title,String kind,String director,String scriptwriter, int duration) throws BadEntryException {
		if(title == null) {
			throw new BadEntryException("Error : Title null");
		}else if(title.replace(" ", "").length()<=1) {
			throw new BadEntryException("Error : Title more trailing blanks");
		}else if(kind == null){
			throw new BadEntryException("Error : Kind null");
		}else if(director == null){
			throw new BadEntryException("Error : Director null");
		}else if(scriptwriter == null){
			throw new BadEntryException("Error : Scriptwriter null");
		}else if(duration <= 0){
			throw new BadEntryException("Error : Duration impossible");
		}
		this.title        = title;
		this.kind         = kind;
		this.director     = director;
		this.scriptwriter = scriptwriter;
		this.duration     = duration;
	}

	/**
	 * @return the ItemFilm's mean mark
	 */

	public LinkedList<Opinion> getList(){
		return theOpinions;
	}

	public float getMeanMark(){ return this.meanMark; }

	/**
	 * @param title title of a film to be tested
	 *
	 * @return true if the title given is already add in the social-network's ItemFilm's List
	 */

	public boolean isIt (String title) {
		return (this.title.trim().equalsIgnoreCase(title.trim()));
	}

	/**
	 * @param comment          comment given by a user
	 * @param mark             mark given by a user
	 * @param memberAddOpinion user that give these two parameters
	 *
	 * @return the mean mark of the Itemfilm
	 *
	 * @throws BadEntryException
	 *
	 * addOpinion checks if the user is a registered member of social-network
	 * if so, it tests if the member already add a comment and a mark :
	 * 	- if not, it add them
	 * 	- if so, it erase the first comment and place the new one
	 */


	public float addOpinion(String comment, float mark, Member memberAddOpinion) throws BadEntryException {
		Opinion opinionTest = new Opinion(comment, mark, memberAddOpinion);
		float sum=0;
		float sumKarma=0f;
		float karmaMember;

		if(theOpinions.size()>=1){
			sumKarma=1f;
		}

		boolean testUpdate=false;

		for (Opinion opinion : theOpinions) {
			karmaMember=opinion.member.getKarma();
			if (opinion.member.areYou(memberAddOpinion.getLogin())) {
				float oldMark = opinion.update(opinionTest.getComment(),opinionTest.getMark());
				sum+=mark * karmaMember;
				testUpdate=true;
			}
			else {
				sum += opinion.getMark() * karmaMember;
			}
		}

		if(testUpdate){
			return this.meanMark=meanFilm(sum,sumKarma+memberAddOpinion.getKarma());
		}
		else {
			theOpinions.add(opinionTest);
			sum += mark * memberAddOpinion.getKarma();
			return this.meanMark = meanFilm(sum, sumKarma + memberAddOpinion.getKarma());
		}
	}

	/**
	 * @return the mean for a Karma
	 */
	public float meanFilm(float sum, float sumKarma){
		float  mean = 0f;
		return mean = sum/sumKarma;
	}
    /**
     * @param memberAlreadyWriteOpinion login of the member who wants to add a review
     * @param commentAddOpinion         comment on wich we had a review
     * @param mark                      mark given by a user
     * @param comment                   comment given by a user
     * @param memberAddReviewOpinion    user that give these parameters
     *
     * @throws BadEntryException
     *
     * addReviewOpinion checks if the user is a registered member of social-network and if the opinion wich we have to comment exists
     * if so, it calculate the mark considering karma.
     */
	public void addReviewOpinion(Member memberAlreadyWriteOpinion, String commentAddOpinion, float mark, String comment, Member memberAddReviewOpinion) throws BadEntryException{
		if(commentAddOpinion == null){
			throw new BadEntryException("Error : Bad Comment");
		}
		boolean testOpinion = false;
		for(Opinion opinion : theOpinions){
			if(opinion.isItOpinionReview(memberAlreadyWriteOpinion.getLogin(),commentAddOpinion)){
				memberAlreadyWriteOpinion.setKarma(opinion.reviewOpinion(comment,mark,memberAddReviewOpinion));
                testOpinion=true;
			}
		}
		if(!testOpinion) {
            throw new BadEntryException("Error : Not in the Opinion List");
        }

		float sum=0;
		float sumKarma=0;
		float karmaMember;


		for (Opinion opinionSum : theOpinions ) {
			karmaMember=opinionSum.member.getKarma();
			sum+=opinionSum.getMark()*karmaMember;
			sumKarma+=karmaMember;
		}

		this.meanMark=meanFilm(sum,sumKarma);
	}

	public String toString(){
		return this.title;
	}
}
