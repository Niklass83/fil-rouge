package opinion;

import exceptions.BadEntryException;

public class Member {
	private String login;
	private String password;
	private String profile;
	private float  karma = 1f;

	public Member(){}

	/**
	 * @param  login
	 * @throws BadEntryException
	 */
	public Member (String login) throws BadEntryException {
		if(login == null) {
			throw new BadEntryException("Error : Login null");
		}
		else if(login.replace(" ", "").length()<=1) {
			throw new BadEntryException("Error : Login more trailing blanks");
		}
		this.login=login;
	}

	/**
	 * @param login
	 * @param password
	 * @throws BadEntryException
	 */
	public Member (String login, String password) throws BadEntryException {
		if(login == null) {
			throw new BadEntryException("Error : Login null");
		}
		else if(login.replace(" ", "").length()<=1) {
			throw new BadEntryException("Error : Login more trailing blanks");
		}
		else if(password == null) {
			throw new BadEntryException("Error : Password null");			
		}
		
		// Verification good entry of password and login without trailing blanks and space
		String password1 = password.trim();
		String login1    = login.trim();
		if(password1.length()<4) {
			throw new BadEntryException("Error : Bad Password");
		}
		if(login1.length()==0) {
			throw new BadEntryException("Error : Bad login");
		}
		
		this.login    = login;
		this.password = password;
	}

	/**
	 * @param login
	 * @param password
	 * @param profile
	 * @throws BadEntryException
	 */
	public Member (String login, String password, String profile) throws BadEntryException {
		if(login == null) {
			throw new BadEntryException("Error : Login null");
		}
		else if(login.replace(" ", "").length()<=1) {
			throw new BadEntryException("Error : Login more trailing blanks");
		}
		else if(password == null) {
			throw new BadEntryException("Error : Password null");			
		}
		else if(profile == null) {
			throw new BadEntryException("Error : Profile Null");
		}
		
		// Verification good entry of password and login without trailing blanks and space
		String password1 = password.trim();
		String login1    = login.trim();
		if(password1.length()<4) {
			throw new BadEntryException("Error : Bad Password");
		}
		if(login1.length()==0) {
			throw new BadEntryException("Error : Bad login");
		}
		this.login    = login;
		this.password = password;
		this.profile  = profile;
	}

	/**
	 * @return Login of the Member
	 */
	public String getLogin() {
		return this.login;
	}


	/**
	 * @return Password of the Member
	 */
	public String getPassword() {
		return this.password;
	}
	
	public String getProfile() {
		return this.profile;
	}

	public float getKarma(){ return this.karma; }
		
	public void setKarma(float karma){
		float karmaAdd = (karma*2)/5;
		float oldKarma = this.getKarma();
		this.karma     = (oldKarma+karmaAdd)/2 ;
	}

	/**
	 * @param login
	 * @return boolean
	 *
	 * areYou check if the login match with a login from a Member
	 *
	 */
	public boolean areYou(String login) {
		String loginTrim=login.trim();
		if(this.login.trim().equalsIgnoreCase(loginTrim)){
			return true;
			}
			return false;
		}

	/**
	 * @param member
	 * @return boolean
	 *
	 * Auth make a comparaison between the param (login, password) and the login and password of a Member
	 */
		// The user who gives his credentials is a member
		public boolean auth(Member member) {
			String loginTrim = member.getLogin().trim();
			if ( this.login.trim().equalsIgnoreCase( loginTrim ) && ( this.password.equals( member.getPassword() ) ) ){
				return true;
			}
			return false;
		}

	public String toString(){
		return "Login : "+this.login+" Karma : "+ this.karma;
	}
}
