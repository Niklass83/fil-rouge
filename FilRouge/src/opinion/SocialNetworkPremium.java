package opinion;

import exceptions.*;

public class SocialNetworkPremium extends SocialNetwork implements ISocialNetworkPremium  {


    public void reviewOpinionFilm(String login, String password, String loginCommentAdd, String commentAddOpinion, String title,
                                  float mark, String comment) throws BadEntryException,
            NotMemberException, NotItemException {
        if(title==null){
            throw new BadEntryException("Error : Title Null");
        }
        if(loginCommentAdd==null){
            throw new BadEntryException("Error : login null");
        }
        else if(loginCommentAdd.replace(" ", "").length()<=1) {
            throw new BadEntryException("Error : Login more trailing blanks");
        }
        Member memberTest = new Member(login, password);

        Member memberAddReviewOpinion = new Member();
        Member memberAlreadyWriteOpinion = new Member();
        boolean testId=false;
        boolean testIdLoginCommentAdd=false;
        for (Member member : theMembers) {
            if(member.auth(memberTest)) {
                memberAddReviewOpinion=member;
                testId = true;
            }

            if(member.areYou(loginCommentAdd)){
                memberAlreadyWriteOpinion=member;
                testIdLoginCommentAdd=true;
            }
        }

        if(!testId || !testIdLoginCommentAdd) {
            throw new NotMemberException("Error : Not Authentified");
        }
        // Creation Membre pour verification si commentaire deja ecrit
        testId=false;
        for (ItemFilm movie : theFilms) {
            if(movie.isIt(title)) {
                movie.addReviewOpinion(memberAlreadyWriteOpinion, commentAddOpinion, mark, comment, memberAddReviewOpinion);
                testId=true;
            }
        }
        if(!testId) {
            throw new NotItemException("Error : Not a Movie");
        }
    }


    public void reviewOpinionBook(String login, String password, String loginCommentAdd, String commentAddOpinion, String title,
                                  float mark, String comment) throws BadEntryException,
            NotMemberException, NotItemException {
        if(title==null){
            throw new BadEntryException("Error : Title Null");
        }
        if(loginCommentAdd==null){
            throw new BadEntryException("Error : login null");
        }
        else if(loginCommentAdd.replace(" ", "").length()<=1) {
            throw new BadEntryException("Error : Login more trailing blanks");
        }
        Member m1 = new Member(login, password);
        Member m2 = new Member();
        boolean testId=false;
        boolean testIdLoginCommentAdd=false;
        for (Member member : theMembers) {
            if(member.auth(m1)) {
                testId=true;

            }
            if(member.areYou(loginCommentAdd)){
                m2=member;
                testIdLoginCommentAdd=true;
            }
        }

        if(!testId || !testIdLoginCommentAdd) {
            throw new NotMemberException("Error : Not Authentified");
        }

        Member memberAddOpinion = new Member(login);
        testId=false;
        for (ItemBook Book : theBooks) {
            if(Book.isIt(title)) {
                Book.addReviewOpinion(m2, commentAddOpinion, mark, comment, memberAddOpinion);
                testId=true;
            }
        }
        if(!testId) {
            throw new NotItemException("Error : Not a Book");
        }
    }

}

